#include <opencv2/opencv.hpp>
#include <opencv2/videoio.hpp>

using namespace cv;

#ifndef FRAMECRUNCHER_FRAME_H
#define FRAMECRUNCHER_FRAME_H

class Video;

class Frame {
protected:
    unsigned long index;
    Mat *frame;
    Vec3b dominantColor;

public:
    Frame( unsigned long index );
    ~Frame();

    void analyze();

    Vec3b getDominantColor();

    unsigned long getIndex();

friend Video;
};


#endif
