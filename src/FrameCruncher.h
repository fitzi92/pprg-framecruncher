#include <thread>
#include <mutex>
#include <condition_variable>
#include <queue>
#include <atomic>
#include "Frame.h"
#include "Video.h"
#include "Image.h"

#ifndef FRAMECRUNCHER_FRAMECRUNCHER_H
#define FRAMECRUNCHER_FRAMECRUNCHER_H

#define NUM_WORKER_THREADS 6

#define MAX_FRAME_QUEUE_LENGTH 160
#define MIN_FRAME_QUEUE_LENGTH 80

#define MAX_LOCAL_FRAME_QUEUE_LENGTH 10
#define MAX_LOCAL_PROCESSED_FRAME_QUEUE_LENGTH 20

using namespace std;

class FrameCruncher {

protected:
    atomic<bool> stopFlag;
    atomic<bool> readyFlag;

    Video *video;

    Image *image;

    // progress window
    bool displayWindow;
    string windowName;

    // queue of frames to be processed
    queue<Frame *> frameQueue;
    mutex frameQueueLock;
    condition_variable frameQueueCondition;
    condition_variable frameQueueEmptyCondition;

    // queue of already processed frames that
    // need to be drawn onto the image and window if necessary
    queue<Frame *> processedFrameQueue;
    mutex processedFrameQueueLock;
    condition_variable processedFrameQueueCondition;

    void readFramesWorker();

    void processFramesWorker();

    void createImageWorker();

public:
    FrameCruncher(string &video);

    FrameCruncher(string &video, string &windowName);

    ~FrameCruncher();

    void startCrunching();

    void stop();

};


#endif
