#include <iostream>
#include <condition_variable>
#include <thread>
#include <chrono>

#include "FrameCruncher.h"
#include "Video.h"

using namespace std;

FrameCruncher::FrameCruncher(string &video) {
    this->displayWindow = false;
    this->stopFlag = false;
    this->readyFlag = false;
    this->video = new Video(video);
}

FrameCruncher::FrameCruncher(string &video, string &windowName) {
    this->displayWindow = true;
    this->windowName = windowName;
    this->stopFlag = false;
    this->readyFlag = false;
    this->video = new Video(video);
}

FrameCruncher::~FrameCruncher() {
    delete this->video;
    delete this->image;

    if (!this->frameQueue.empty()) {
        cout << "frameQueue is not empty" << endl;
    }

    if (!this->processedFrameQueue.empty()) {
        cout << "processedFrameQueue is not empty" << endl;
    }

}

/**
 * Step 1:
 * Extracts frame by frame out of the video.
 * This step can not be parallelized, because a video can only be
 * read frame by frame (due to codecs relying on previous (and sometimes advancing) frames.
 *
 * This process is typically fast. Faster than the image processing thread, therefore this
 * thread limits itself to a max frame queue length, to avoid loading all frames of a video
 * into RAM.
 */
void FrameCruncher::readFramesWorker() {

    queue<Frame *> localQueue;
    unsigned long frameQueueSize = 0;
    unsigned long frameIndex = 0;

    do {
        unique_lock<mutex> uniqueGlobalQueueLock(this->frameQueueLock);

        while (this->frameQueue.size() > MIN_FRAME_QUEUE_LENGTH) {
            this->frameQueueCondition.wait(uniqueGlobalQueueLock);
        }

        frameQueueSize = this->frameQueue.size();

        // unlock
        uniqueGlobalQueueLock.unlock();

        // an fill local queue first, because this may take a while and the global queue
        // should not be blocked the whole time
        while (frameQueueSize < MAX_FRAME_QUEUE_LENGTH && frameIndex < this->video->getLength()) {
            auto nextFrame = this->video->getNextFrame();
            if (nextFrame == nullptr) {
                cout << "Next frame is null, something is wrong" << endl;
            }

            localQueue.push(nextFrame);
            frameIndex++;
            frameQueueSize++;
        }

        // lock again
        uniqueGlobalQueueLock.lock();

        // and shuffle everything from local queue to global queue
        while (!localQueue.empty()) {
            this->frameQueue.push(localQueue.front());
            localQueue.pop();
        }

        uniqueGlobalQueueLock.unlock();
        this->frameQueueEmptyCondition.notify_all();

    } while (frameIndex < this->video->getLength());

    this->readyFlag = true;

    // just in case someone is still sleeping
    this->frameQueueEmptyCondition.notify_all();
    this->processedFrameQueueCondition.notify_all();

    cout << "Reader finished" << endl;

}

/**
 * Step 2:
 * Processes a frame - aka extracts the dominant color of the frame (kmeans)
 * This can be parallelized and is the heavy-lifting of frame cruncher.
 * Each of the these workers has a local queue, which is filled up and worked off
 * to limit global queue locks and non-paralell regions.
 */
void FrameCruncher::processFramesWorker() {

    queue<Frame *> localQueue;
    queue<Frame *> localProcessedQueue;

    bool isFinished = false;

    do {

        // fill local queue if empty
        if (localQueue.empty() && !isFinished) {
            unique_lock<mutex> uniqueGlobalQueueLock(this->frameQueueLock);

            while (!this->frameQueue.empty() && localQueue.size() < MAX_LOCAL_FRAME_QUEUE_LENGTH) {
                localQueue.push(this->frameQueue.front());
                this->frameQueue.pop();
            }

            if (localQueue.size() < MAX_LOCAL_FRAME_QUEUE_LENGTH && !this->readyFlag) {
                cout << "worker thread starved for frames" << endl;
                this->frameQueueEmptyCondition.wait(uniqueGlobalQueueLock);
            }

            // if finished flag is is set and frame queue is still empty
            // everything has been worked off
            if (this->readyFlag && this->frameQueue.empty() && localQueue.empty()) {
                isFinished = true;
            }

            uniqueGlobalQueueLock.unlock();

            this->frameQueueCondition.notify_all(); // notify whenever something was taken
        }

        // work off local queue
        if (!localQueue.empty()) {
            auto frame = localQueue.front();
            localQueue.pop();

            frame->analyze();

            localProcessedQueue.push(frame);
        }

        // transfer local result queue to global one
        if (localProcessedQueue.size() >= MAX_LOCAL_PROCESSED_FRAME_QUEUE_LENGTH || isFinished) {
            unique_lock<mutex> globalProcessedQueueLockGuard(this->processedFrameQueueLock);

            while (!localProcessedQueue.empty()) {
                this->processedFrameQueue.push(localProcessedQueue.front());
                localProcessedQueue.pop();
            }

            globalProcessedQueueLockGuard.unlock();
            this->processedFrameQueueCondition.notify_all();
        }

    } while (!isFinished);

    cout << "Worker finished" << endl;

}

/**
 * Step 3:
 * The last step, it takes analyzed frames, gets the color and draws the picture.
 * This step is the fastest. Therefore it waits if the process frame queue is empty
 * and waits for a singal of a worker thread
 */
void FrameCruncher::createImageWorker() {

    queue<Frame *> localQueue;
    unsigned long frameIndex = 0;

    bool isFinished = false;

    do {

        // fill up local queue if empty
        if (localQueue.empty()) {
            unique_lock<mutex> globalProcessedQueueLockGuard(this->processedFrameQueueLock);

            while (this->processedFrameQueue.empty() && !this->readyFlag) {
                cout << "Sleep" << endl;
                this->processedFrameQueueCondition.wait(globalProcessedQueueLockGuard);
                cout << "Wakeup" << endl;
            }

            //cout << "Work global" << endl;
            while (!this->processedFrameQueue.empty()) {
                localQueue.push(this->processedFrameQueue.front());
                this->processedFrameQueue.pop();
            }

            /*if( this->processedFrameQueue.empty() && localQueue.empty() && this->readyFlag ){
                isFinished = true;
            }*/

        }

        // cout << "Work local" << endl;
        // work off local queue
        while (!localQueue.empty()) {

            auto frame = localQueue.front();
            localQueue.pop();

            this->image->drawFrame(frame);

            frameIndex++;

            cout << "Drawn Frame " << frame->getIndex() << " (" << frameIndex << "/" << this->video->getLength() << ")"
                 << endl;

            delete frame;
        }

        if (this->displayWindow) {
            this->image->display(this->windowName);
            waitKey(1);
        }

    } while (frameIndex < this->video->getLength() && !isFinished);

    cout << "Image drawer finished" << endl;

    if (frameIndex != this->video->getLength()) {
        cout << "But frame index is only " << frameIndex << this->video->getLength() << endl;
    }

}

void FrameCruncher::startCrunching() {

    // open video
    if (!this->video->load()) {
        cout << "Video could not be loaded" << endl;
        return;
    }

    cout << "Video length: " << this->video->getLength() << endl;

    this->image = new Image((int) this->video->getLength(), 500);

    if (this->displayWindow) {
        namedWindow(this->windowName, WINDOW_NORMAL);
        resizeWindow(this->windowName, 1900, 800);
        this->image->display(this->windowName);
        waitKey(1);
    }

    auto startTime = std::chrono::high_resolution_clock::now();

    thread readFramesThread(&FrameCruncher::readFramesWorker, this);
    thread createImageThread(&FrameCruncher::createImageWorker, this);
    vector<thread> processFramesThreads(NUM_WORKER_THREADS);

    for (unsigned int i = 0; i < NUM_WORKER_THREADS; i++) {
        processFramesThreads[i] = thread(&FrameCruncher::processFramesWorker, this);
    }

    readFramesThread.join();
    createImageThread.join();

    for (auto &processFramesThread : processFramesThreads) {
        processFramesThread.join();
    }

    auto endTime = std::chrono::high_resolution_clock::now();
    auto duration = chrono::duration_cast<chrono::seconds>(endTime - startTime).count();

    // print time
    cout << "Video processed in " << duration << " seconds" << endl;

    this->image->save("result.jpg");

    if (this->displayWindow) {
        this->image->display(this->windowName);
        waitKey(0);
    }

}