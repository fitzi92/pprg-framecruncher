# FrameCruncher
FrameCruncher is a tool that let's you display a summary
of the dominant color of each frame of a movie.

## Info

Sometimes in the future the following things will be paramters (hopefully).

Until then, please do:
- Change the path to your video file in main.cpp
- Result.jpg will be generated in the working directory

## Build

Create build directory
```bash
mkdir build
```

Change into it
```bash
cd build
```

Run cmake
```bash
cmake ..
```

Run make
```bash
make
```

Run FrameCruncher
```bash
./FrameCruncher
```

## Install dependencies
1\. Install the _GTK+ OpenGL Extensions_

```bash
sudo apt-get install libgtkglext1 libgtkglext1-dev
```


2\. Install OpenCV

Follow the official guide:

https://docs.opencv.org/master/d7/d9f/tutorial_linux_install.html

Make sure to build with OpenGL (Step 2 in the building process):
``` bash
cmake -D CMAKE_BUILD_TYPE=Release -D WITH_OPENGL=ON -D CMAKE_INSTALL_PREFIX=/usr/local ..
```


3\. Build and Execute Frame Cruncher

:)